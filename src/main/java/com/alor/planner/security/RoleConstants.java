package com.alor.planner.security;

public final class RoleConstants {

    public static String ADMIN = "ROLE_ADMIN";

    public static String USER = "ROLE_USER";

    public static String ANONYMOUS = "ROLE_ANONYMOUS";

    public RoleConstants() { }
}

package com.alor.planner.service;

import com.alor.planner.domain.Role;
import com.alor.planner.domain.User;

import java.util.List;

public interface UserService {

    User registerUser(User user);

    List<Role> getRoles();
}

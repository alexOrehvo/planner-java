package com.alor.planner.service;

import com.alor.planner.domain.Event;

import java.util.List;

public interface EventService {

    List<Event> findAll();

    Event save(Event event);

    void delete(long id);
}

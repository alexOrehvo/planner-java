package com.alor.planner.service.impl;

import com.alor.planner.dao.UserDao;
import com.alor.planner.domain.Role;
import com.alor.planner.domain.User;
import com.alor.planner.security.RoleConstants;
import com.alor.planner.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User registerUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEmail(user.getEmail().toUpperCase());


        Set<Role> roles = new HashSet<>();
        roles.add(new Role(RoleConstants.USER));
        user.setRoles(roles);

        return userDao.add(user);
    }

    @Override
    public List<Role> getRoles() {
        List<Role> roles = new ArrayList<>();
        roles.add(new Role(RoleConstants.USER));
        roles.add(new Role(RoleConstants.ADMIN));
        roles.add(new Role(RoleConstants.ANONYMOUS));
        return roles;
    }


}

package com.alor.planner.service.impl;

import com.alor.planner.dao.EventDao;
import com.alor.planner.domain.Event;
import com.alor.planner.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    private EventDao eventDao;

    public EventServiceImpl(@Autowired EventDao eventDao) {
        this.eventDao = eventDao;
    }

    @Override
    public List<Event> findAll() {
        return eventDao.findAll();
    }

    @Override
    public Event save(Event event) {
        return eventDao.save(event);
    }

    @Override
    public void delete(long id) {
        eventDao.delete(id);
    }
}

package com.alor.planner.dao.impl;

import com.alor.planner.dao.UserDao;
import com.alor.planner.domain.User;
import org.hibernate.SessionFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    private List<User> users = new ArrayList<>();

    private SessionFactory sessionFactory;

    private PasswordEncoder passwordEncoder;

    public UserDaoImpl(PasswordEncoder passwordEncoder, SessionFactory sessionFactory) {
        this.passwordEncoder = passwordEncoder;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User findByEmail(String email) {
        for (User user : users) {
            if (user.getEmail().equals(email)) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public void delete(long id) {

    }
}

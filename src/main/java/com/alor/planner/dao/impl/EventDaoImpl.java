package com.alor.planner.dao.impl;


import com.alor.planner.dao.EventDao;
import com.alor.planner.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EventDaoImpl implements EventDao{

    @Override
    public List<Event> findAll() {
        return new ArrayList<>();
    }

    @Override
    public Event getOne(long id) {
        return new Event();
    }

    @Override
    public Event save(Event event) {
        return new Event();
    }

    @Override
    public Event delete(long id) {
        return new Event();
    }

    @Override
    public Event update(Event event) {
        return new Event();
    }
}

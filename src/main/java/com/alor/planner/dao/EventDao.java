package com.alor.planner.dao;

import com.alor.planner.domain.Event;

import java.sql.SQLException;
import java.util.List;

public interface EventDao {

    List<Event> findAll();

    Event getOne(long id);

    Event save(Event event);

    Event delete(long id);

    Event update(Event event);
}

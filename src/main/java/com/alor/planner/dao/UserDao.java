package com.alor.planner.dao;

import com.alor.planner.domain.User;

public interface UserDao {

    User findByEmail(String email);

    User add(User user);

    void delete(long id);
}

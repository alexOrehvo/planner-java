package com.alor.planner.controller;

import com.alor.planner.domain.User;
import com.alor.planner.service.UserService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/api")
@RestController
public class AccountController {

    private UserService userService;

    private List<User> users = new ArrayList<>();

    public AccountController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public List<User> view() {
        return users;
    }

    @PostMapping("/registry")
    public void register(@Valid @RequestBody User user, BindingResult bindingResult) {
        System.out.println(user.toString());
        if (!bindingResult.hasErrors()) {
            userService.registerUser(user);
            System.out.println("Send an activation email");
        }
    }

    @GetMapping("/delete")
    public List<User> delete() {
        return users;
    }

    @GetMapping("/del")
    public List<User> del() {
        return users;
    }


}
